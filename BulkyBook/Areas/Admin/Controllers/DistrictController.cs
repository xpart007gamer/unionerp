﻿using Microsoft.AspNetCore.Mvc;
using Union_ERP.DataAccess;
using Union_ERP.DataAccess.Repository.IRepository;
using Union_ERP.Models;

namespace Union_ERP.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class DistrictController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IWebHostEnvironment _hostEnvironment;

        public DistrictController(IUnitOfWork unitOfWork, IWebHostEnvironment hostEnvironment)
        {
            this._unitOfWork = unitOfWork;
            this._hostEnvironment = hostEnvironment;
        }


        public IActionResult Add()
        {


            return View();
        }

        public IActionResult Edit(int? Id)
        {
            var district = _unitOfWork.District.GetFirstOrDefault(f => f.Id == Id);

            return View(district);
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(District model)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.District.Add(model);
                _unitOfWork.Save();
            }

            return RedirectToAction("Index");
        }
        [HttpPost]
        public IActionResult Update(District model)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.District.UpdateDistrict(model);
                _unitOfWork.Save();
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var distrcitList = _unitOfWork.District.GetAll();

            return Json(new { data = distrcitList });
        }


        [HttpDelete]
        public IActionResult Delete(int? id)
        {

          
            var district = _unitOfWork.District.GetFirstOrDefault(f => f.Id == id);
            if (district == null)
            {
                return Json(new { success = false, message = "Error while Deleting" });
            }
          
            _unitOfWork.District.Remove(district);
            _unitOfWork.Save();
            return Json(new { success = true, message = "Deleted Successfully!" });
        }

    }
}
