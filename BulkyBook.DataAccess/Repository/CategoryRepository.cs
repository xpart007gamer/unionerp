﻿using Union_ERP.DataAccess.Repository.IRepository;
using Union_ERP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Union_ERP.DataAccess.Repository
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        private ApplicationDbContext _db;

        public CategoryRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
      
        public void Update(Category category)
        {
            _db.Categories.Update(category);
        }
    }
}
