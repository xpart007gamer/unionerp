﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Union_ERP.Models;

namespace Union_ERP.DataAccess.Repository.IRepository
{
    public interface IDistrictRepository : IRepository<District>
    {
        void UpdateDistrict(District district);
    }
}
