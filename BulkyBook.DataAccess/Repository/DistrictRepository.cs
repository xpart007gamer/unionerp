﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Union_ERP.DataAccess.Repository.IRepository;
using Union_ERP.Models;

namespace Union_ERP.DataAccess.Repository
{
    public class DistrictRepository : Repository<District>, IDistrictRepository
    {
        private ApplicationDbContext _db;

        public DistrictRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
        void IDistrictRepository.UpdateDistrict(District district)
        {
            _db.District.Update(district);
            _db.SaveChanges();
        }
    }
}
