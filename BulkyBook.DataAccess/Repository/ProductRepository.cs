﻿using Union_ERP.DataAccess.Repository.IRepository;
using Union_ERP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Union_ERP.DataAccess.Repository
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        private ApplicationDbContext _db;

        public ProductRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
      
        public void Update(Product product)
        {
          
            //var data = _db.Products.FirstOrDefault(f => f.Id == product.Id);
            //if(data != null)
            //{
            //    product.Name = data.Name;
            //    product.Description = data.Description;
            //    product.Price = data.Price;
            //    product.CategoryId = data.CategoryId;
            //    product.Author = data.Author;
            //    product.CoverTypeId = data.CoverTypeId;

            //    if(product.ImageUrl != null)
            //    {
            //        product.ImageUrl = product.ImageUrl.Trim();
            //    }
            //}
            _db.Products.Update(product);
        }

       
    }
}
