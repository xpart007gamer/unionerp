﻿using Union_ERP.DataAccess.Repository.IRepository;
using Union_ERP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Union_ERP.DataAccess.Repository
{
    public class CompanyRepository : Repository<Company>, ICompanyRepository
    {
        private ApplicationDbContext _db;

        public CompanyRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
      
        public void Update(Company coverType)
        {
            _db.Companies.Update(coverType);
        }
    }
}
