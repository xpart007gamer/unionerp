﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Union_ERP.Models.ViewModels
{
    public class ProductVM
    {
        public Product Product { get; set; }
        
        public IFormFile Image { get;set; }
        public IEnumerable<SelectListItem>? CategoryList { get; set; }
        public IEnumerable<SelectListItem>? CoverTypeList { get; set; }

        //var product = new Product();
        //IEnumerable<SelectListItem> CategoryList = _unitOfWork.Category.GetAll().Select(s => new SelectListItem { Text = s.Name, Value = s.Id.ToString() });
        //IEnumerable<SelectListItem> CoverTypeList = _unitOfWork.CoverType.GetAll().Select(s => new SelectListItem { Text = s.Name, Value = s.Id.ToString() });

    }
}
