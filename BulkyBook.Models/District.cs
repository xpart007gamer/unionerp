﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Union_ERP.Models
{
    public class District
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
